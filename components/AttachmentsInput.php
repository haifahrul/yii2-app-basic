<?php
namespace app\components;

use yii\helpers\Url;
use nemmo\attachments\components\AttachmentsInput as BaseAttachmentsInput;

class AttachmentsInput extends BaseAttachmentsInput
{
    public function run()
    {
        $this->pluginOptions['uploadUrl'] = Url::toRoute('/attachments/file/upload', true);
        return parent::run();
    }
}
