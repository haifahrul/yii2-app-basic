<?php

use yii\db\Migration;

class m190509_110823_create_table_tbl_session extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%session}}', [
            'id' => $this->char(40)->notNull()->append('PRIMARY KEY'),
            'expire' => $this->integer(),
            'data' => $this->binary(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%session}}');
    }
}
