<?php
namespace app\rbac;

use yii\rbac\Rule;
use app\models\Launch;

/**
 * Checks if created_by matches user passed via params
 */
class AuthorRule extends Rule
{
    public $name = 'isAuthor';

    /**
     * @param string|int $userId the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($userId, $item, $params)
    {
        return isset($params['post']) ? $params['post']->created_by == $userId : false;
    }
}
