# User Manual

This is the user manual for the **Launch Markets Tracking** application.

See also the [Keyword Index](@web/site/help?page=INDEX).

## General Usage

  * [First Time Usage](@web/site/help?page=General-Usage-First-Time)
  * [Opening the Application](../site/help?page=General-Usage-Open-Application)
  * [Changing User Settings](../site/help?page=General-Usage-Change-User-Settings)
  * [More Help](../site/help?page=General-Usage-More-Help)

## Market Users

* [Viewing Launches](../site/help?page=Market-User-View-Launches)
* [Updating Action Items](../site/help?page=Market-User-Update-Action-Items)
* [Batch Updating Action Items](../site/help?page=Market-User-Batch-Update-Action-Items)
* [Viewing all Markets](../site/help?page=Market-User-Viewing-All-Markets)
* [Sending Update Requests](../site/help?page=Market-User-Send-Update-Requests)

## Reports

* [Overview](@web/site/help?page=Report-Overview)
* [Action Item Status by Markets](@web/site/help?page=Report-Action-Item-Status-By-Markets)
* [Markets Progress Report](@web/site/help?page=Report-Markets-Progress-Report)
* [Volumes by Markets](@web/site/help?page=Report-Volumes-By-Markets)
* [Action Items by Status](@web/site/help?page=Report-Action-Items-By-Status)

## Central Launch Management

* [Managing Launches](@web/site/help?page=Central-Manage-Launches)
* [Adding Attachments to Launches](@web/site/help?page=Central-Add-Attachments-To-Launches)
* [Adding Action Items](@web/site/help?page=Central-Add-Action-Items)
* [Adding Markets](@web/site/help?page=Central-Add-Markets)
* [Synchronizing Markets via Excel Download/Upload](@web/site/help?page=Central-Excel-Down-Upload)

## Admin Manual

  * [Creating a New User Account](@web/site/help?page=Admin-Create-New-User-Account)
  * [Managing User Accounts](@web/site/help?page=Admin-Manage-User-Accounts)
  * [Manage Lookup Values](@web/site/help?page=Admin-Lookup-Values)
  * [Change Site Settings](@web/site/help?page=Admin-Change-Site-Settings)


