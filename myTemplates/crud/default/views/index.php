<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
<?= $generator->indexWidgetType === 'grid' ? "use yii\\web\\View;\n" : '' ?>
use <?= $generator->indexWidgetType === 'grid' ? "\\app\\widgets\\GridView" : "yii\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>;
$this->params['breadcrumbs'][] = $this->title;
$this->context->leftMenu = [
    [
        'label' => <?= $generator->generateString('Operations') ?>,
        'items' => [
            [ 'label' => <?= $generator->generateString('New ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, 'url' => ['create']],
        ],
    ],
];
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">

    <h1><?= "<?= " ?>Html::encode($this->title) ?></h1>
<?php if(!empty($generator->searchModelClass)): ?>
<?= "    <?php " . ($generator->indexWidgetType === 'grid' ? "// " : "") ?>echo $this->render('_search', ['model' => $searchModel]); ?>
<?php endif; ?>

    <?= "<?php " ?>// {{{ Clear Filters Form ?>
    <?= "<?php " ?> $form = Html::beginForm(['index'], 'post', ['id'=>'clear-filters-form', 'style'=>'display:inline']); ?>
    <?= "<?php " ?> $form .= Html::hiddenInput('clear-state', '1'); ?>
    <?= "<?php " ?> $form .= Html::hiddenInput('redirect-to', ''); ?>
    <?= "<?php " ?> $form .= Html::endForm(); ?>
    <?= "<?php " ?> echo $form; /* }}} */ ?>

    <p>
        <?= "<?= " ?>Html::a('<span class="glyphicon glyphicon-plus"></span>&nbsp;'.<?= $generator->generateString('New ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, ['create'], ['class' => 'btn btn-success']) ?>
        <?= "<?= " ?>Html::a('<span class="glyphicon glyphicon-refresh"></span>&nbsp;'.Yii::t('app', 'Clear Filters'), ['#'], ['class' => 'btn btn-success', 'id'=>'clearFiltersBtn'] ) ?>
    </p>

<?= $generator->enablePjax ? "    <?php Pjax::begin(); ?>\n" : '' ?>
<?php if ($generator->indexWidgetType === 'grid'): ?>
    <?= "<?= " ?>GridView::widget([ // {{{ 
        'dataProvider' => $dataProvider,
        'as filterBehavior' => \thrieu\grid\FilterStateBehavior::className(),
        <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n        'columns' => [\n" : "'columns' => [\n"; ?>
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}&nbsp;{update}',
                'contentOptions' => [ 'class' => 'text-center' ],
            ],
            ['class' => 'yii\grid\SerialColumn'],

<?php
$count = 0;
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        if (++$count < 6) {
            echo "            '" . $name . "',\n";
        } else {
            echo "            //'" . $name . "',\n";
        }
    }
} else {
    foreach ($tableSchema->columns as $column) {
        $format = $generator->generateColumnFormat($column);
        // echo "            // column: ".\yii\helpers\VarDumper::dumpAsString($column)."\n";
        if($column->dbType=='tinyint(1)')
            $format='boolean';
        if (++$count < 6) {
            echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        } else {
            echo "            //'" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        }
    }
}
?>

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'contentOptions' => [ 'class' => 'text-center' ],
            ],
        ],
    ]); /* }}} */?>
<?php else: ?>
    <?= "<?= " ?>ListView::widget([
        'dataProvider' => $dataProvider,
        // 'as filterBehavior' => \thrieu\grid\FilterStateBehavior::className(),
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
        },
    ]) ?>
<?php endif; ?>
<?= $generator->enablePjax ? "    <?php Pjax::end(); ?>\n" : '' ?>
</div>

<?php if ($generator->indexWidgetType === 'grid'): ?>
<?= "<?php " ?>$this->registerJs("
$('#clearFiltersBtn').on('click', function() { 
    $('#clear-filters-form').submit();
});
", View::POS_READY, 'my-button-handler' ); ?>
<?php endif; ?>
