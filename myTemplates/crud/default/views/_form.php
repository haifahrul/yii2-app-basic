<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

    <?= "<?php " ?>$form = ActiveForm::begin(['layout' => 'horizontal']); ?>

<?php foreach ($generator->getColumnNames() as $attribute) {
    if (in_array($attribute, $safeAttributes)) {
        echo "    <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
    }
} ?>

    <div class="form-group">
        <label class="control-label col-sm-3"></label>
        <div class="col-sm-6">
            <?= "<?= " ?>Html::submitButton('<span class="glyphicon glyphicon-ok"></span> ' . <?= $generator->generateString('Save') ?>, ['class' => 'btn btn-success']) ?>
            <?= "<?= " ?>Html::submitButton('<span class="glyphicon glyphicon-fast-backward"></span> ' . <?= $generator->generateString('Reset') ?>, ['class' => 'btn btn-primary']) ?>
        </div>
    </div>


    <?= "<?php " ?>ActiveForm::end(); ?>

</div>
